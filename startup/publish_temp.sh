#!/usr/bin/env bash

: ${REDIS_HOST=localhost}
: ${REDIS_CHAN=covis.therm}
: ${THERM_DEV=/dev/ttyMI4}

[[ -e /etc/cron-env ]] && source /etc/cron-env
[[ -e $THERM_DEV ]] && readtemp --redis ${REDIS_HOST}:6379,${REDIS_CHAN} $THERM_DEV
