#!/usr/bin/env bash
#
# Load a COVIS sweep definition into the sweeprunner task queue.
#
: ${NETWORK="covis_covisnet"}

if [[ -z $RS_CONFIG ]]; then
    if [[ -s ./sweeps.toml ]]; then
        RS_CONFIG="$PWD/sweeps.toml"
    else
        RS_CONFIG="$HOME/config/sweeps.toml"
    fi
fi

[[ -s $RS_CONFIG ]] || {
    echo "No sweeps definition file found"
    exit 1
}

docker run --rm -it \
       --network $NETWORK \
       -v "$RS_CONFIG:/config/sweeps.toml" \
       runsweep:latest \
       --redis redis:6379 --config /config/sweeps.toml "$@"
