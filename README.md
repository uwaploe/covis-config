# COVIS Configuration Files

This repository contains various configuration files for COVIS.

- sweeps.toml : sweep definition file.
- datacollect/docker-compose.yaml : Docker Compose file for data
  collection services.
- datacollect/rpc.toml : configuration for Rotator gRPC server.
- datacollect/system.toml : configuration file for the Sweep Runner
  service.
